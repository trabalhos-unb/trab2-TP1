#ifndef STUBS_H_INCLUDED
#define STUBS_H_INCLUDED

#include "Interfaces.h"
#include <stdexcept>
#include <iostream>
#include <typeinfo>

using namespace std;

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe stub da interface ILNAutenticacao.

class StubLNAutenticacao:public ILNAutenticacao{  // classe implementa interface.

public:

    // Defini��o de valor a ser usado como gatilho para falha.

    const static int TRIGGER_FALHA        = 67890;
    const static int TRIGGER_ERRO_SISTEMA = 78901;

    // Declara��o de m�todo previsto na interface.

    ResultadoAutenticacao autenticar(const Matricula&, const Senha&) throw(runtime_error);
};

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe stub da interface ILNProjeto.

class StubLNProjeto:public ILNProjeto{  // classe implementa a interface.

public:
    ResultadoProjeto incluir(const Projeto&) throw(runtime_error);
    ResultadoProjeto remover(const Codigo&) throw(runtime_error);
    ResultadoProjeto pesquisar(const Codigo&) throw(runtime_error);
    ResultadoProjeto editar(const Projeto&) throw(runtime_error);
};

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe stub da interface ILNGerente.

class StubLNGerente:public ILNGerente{  // classe implementa a interface.

public:
    ResultadoGerente incluir(const Gerente&) throw(runtime_error);
    ResultadoGerente remover(const Matricula&) throw(runtime_error);
    ResultadoGerente pesquisar(const Matricula&) throw(runtime_error);
    ResultadoGerente editar(const Gerente&) throw(runtime_error);
};


// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe stub da interface IPersistencia.

class StubPersistencia:public IPersistencia {

public:

    void executar (const ComandoBancoDados*) throw(runtime_error);
};


#endif // STUBS_H_INCLUDED
