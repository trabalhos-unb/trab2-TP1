#ifndef CONTROLADORAS_H_INCLUDED
#define CONTROLADORAS_H_INCLUDED

#include "interfaces.h"
#include "dominios.h"

#include <stdexcept>
#include <iostream>
#include <cstdlib>

using namespace std;

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe controladora de intera��o para o servi�o de autentica��o.

class CntrIUAutenticacao:public IUAutenticacao {    // observe que a classe implementa interface.

private:

    // Refer�ncia para servidor.

    ILNAutenticacao *cntrLNAutenticacao;

public:

    // M�todo previsto na interface.

    ResultadoAutenticacao autenticar() throw(runtime_error);

    // M�todo por meio do qual � estabelecido relacionamento com o servidor.

    void setCntrLNAutenticacao(ILNAutenticacao *cntrLNAutenticacao){
        this->cntrLNAutenticacao = cntrLNAutenticacao;
    }
};

// EXEMPLO
// --------------------------------------------------------

// Exemplifica controladora com m�ltiplos servi�os.

class CntrIUProjeto:public IUProjeto {

private:

    const static int INCLUIR   = 1;
    const static int REMOVER   = 2;
    const static int PESQUISAR = 3;
    const static int EDITAR    = 4;
    const static int RETORNAR  = 5;

    // Refer�ncia para servidor.

    ILNProjeto *cntrLNProjeto;

    // M�todos respons�veis por prover os servi�os.

    void incluir() throw(runtime_error);
    void remover() throw(runtime_error);
    void pesquisar() throw(runtime_error);
    void editar() throw(runtime_error);

public:

    // M�todo previsto na interface.

    void executar(const Matricula&) throw(runtime_error);

    // M�todo por meio do qual � estabelecido relacionamento com o servidor.

    void setCntrLNProjeto(ILNProjeto *cntrLNProjeto){
        this->cntrLNProjeto = cntrLNProjeto;
    }
};

// EXEMPLO
// --------------------------------------------------------

// Exemplifica controladora com m�ltiplos servi�os providos por outras controladoras.

// Exemplifica classe interna.

class CntrIUGerente:public IUGerente {

private:

    const static int INCLUIR   = 1;
    const static int REMOVER   = 2;
    const static int PESQUISAR = 3;
    const static int EDITAR    = 4;
    const static int RETORNAR  = 5;

    // Declara��es adiantadas das classes internas.

    class CntrEdicao;
    class CntrInclusao;
    class CntrPesquisa;
    class CntrRemocao;

    // Refer�ncia para servidor.

    ILNGerente *cntrLNGerente;

    // M�todos respons�veis por prover os servi�os.

    void incluir() throw(runtime_error);
    void remover() throw(runtime_error);
    void pesquisar() throw(runtime_error);
    void editar() throw(runtime_error);

public:

    // M�todo previsto na interface por meio do qual � solicitada execu��o da controladora.

    void executar(const Matricula&) throw(runtime_error);

    // M�todo por meio do qual � estabelecido relacionamento com o servidor.

    void setCntrLNGerente(ILNGerente *cntrLNGerente){
        this->cntrLNGerente = cntrLNGerente;
    }
};

// Declara��es de classes internas para presta��o de servi�o.

class CntrIUGerente::CntrInclusao{
    public:
        void executar(ILNGerente *) throw(runtime_error);
};

class CntrIUGerente::CntrRemocao{
    public:
        void executar(ILNGerente *) throw(runtime_error);
};

class CntrIUGerente::CntrPesquisa{
    public:
        void executar(ILNGerente *) throw(runtime_error);
};

class CntrIUGerente::CntrEdicao{
    public:
        void executar(ILNGerente *)throw(runtime_error);
};


// EXEMPLO
// --------------------------------------------------------

// Exemplifica controladora segundo padr�o Singleton.

// Apenas c�digo necess�rio ao padr�o Singleton foi inclu�do.

// Em uma implementa��o real, o resto do c�digo seria necess�rio.


class CntrIUDesenvolvedor:public IUDesenvolvedor {
    private:
        ILNDesenvolvedor *cntrLNDesenvolvedor;

        // Refer�ncia para a inst�ncia.
        static CntrIUDesenvolvedor *referencia;

        // M�todo construtor privado inibe a cria��o de objetos por m�todos n�o membros da classe.
        CntrIUDesenvolvedor(){}
    public:
        static CntrIUDesenvolvedor *instanciar(){
            if(referencia == 0){
                referencia = new CntrIUDesenvolvedor();
            }
            return referencia;
        }

        // M�todos previstos na interface.
        void executar(const Matricula&) throw(runtime_error);
        void setCntrLNDesenvolvedor(ILNDesenvolvedor*);
};



#endif // CONTROLADORAS_H_INCLUDED
