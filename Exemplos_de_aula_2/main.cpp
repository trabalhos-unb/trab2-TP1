// Exemplos desenvolvidos para ilustrar a sintaxe da linguagem.

#include <stdexcept>
#include <iostream>
#include <string>

#include "dominios.h"
#include "entidades.h"
#include "testes.h"
#include "interfaces.h"
#include "controladoras.h"
#include "stubs.h"
#include "builders.h"

using namespace std;

// Inclus�o de declara��es de classes.

int main()
{

// EXEMPLO
// --------------------------------------------------------

// Liga��o entre controladora de intera��o e stub de neg�cio.

    // Instancia controladoras.

    IUAutenticacao  *cntrIUAutenticacao = new CntrIUAutenticacao();
    ILNAutenticacao *stubLNAutenticacao = new StubLNAutenticacao();

    // Liga (link) inst�ncia de controladora a inst�ncia de stub.

    cntrIUAutenticacao->setCntrLNAutenticacao(stubLNAutenticacao);

    // Para facilitar o teste informa os valores inv�lidos.

    cout << endl << "VALORES DOS TRIGGERS:" << endl << endl;
    cout << "Matricula invalida          = " << Matricula::MATRICULA_INVALIDA << endl;
    cout << "Senha invalida              = " << Senha::SENHA_INVALIDA << endl;
    cout << "Trigger de falha            = " << StubLNAutenticacao::TRIGGER_FALHA << endl;
    cout << "Trigger de erro de sistema  = " << StubLNAutenticacao::TRIGGER_ERRO_SISTEMA << endl;

    // Solicita execu��o de servi�o � controladora de intera��o.

    ResultadoAutenticacao resultado;

    while(true){

        cout << endl << "Tela de apresentacao de sistema." << endl;

        try{
            // Solicita servi�o de autentica��o.

            resultado = cntrIUAutenticacao->autenticar();
        }
        catch(const runtime_error &exp){
                 cout << "Erro de sistema." << endl;
        }

        // Critica o resultado da autentica��o.

        if(resultado.getValor() == ResultadoAutenticacao::SUCESSO) {
            break;
        }
    }

    // Acessa matr�cula retornada da autentica��o.

    Matricula matricula = resultado.getMatricula();

// EXEMPLO
// --------------------------------------------------------

    // Cria refer�ncia para a controladora de interface com o usu�rio projeto.

    IUProjeto  *cntrIUProjeto   = new CntrIUProjeto();

    ILNProjeto *stubLNProjeto = new StubLNProjeto();

    // Liga (link) inst�ncia de controladora a inst�ncia de stub.

    cntrIUProjeto->setCntrLNProjeto(stubLNProjeto);

    try{
        cntrIUProjeto->executar(matricula);
    }
    catch(const runtime_error &exp){
        cout << "Erro de sistema." << endl;
    }

// EXEMPLO
// --------------------------------------------------------

    // Ilustra padr�o Builder na interliga��o de subsistema no modo teste.

    BuilderSubsistemaGerenteTeste builder;

    IUGerente *cntrIUGerente;

    cntrIUGerente = builder.construir();

    try{
        cntrIUGerente->executar(matricula);
    }
    catch(const runtime_error &exp){
        cout << "Erro de sistema." << endl;
    }

// EXEMPLO
// --------------------------------------------------------

// Ilustra constru��o de objeto Singleton.

    CntrIUDesenvolvedor *cntrIUDesenvolvedorA, *cntrIUDesenvolvedorB;

    // O seguinte enunciado provoca erro pois o construtor � privado.

    // cntrIUDesenvolvedorA = new CntrIUDesenvolvedor();

    // Os enunciados seguintes criam o objeto via m�todo.

    cntrIUDesenvolvedorA = CntrIUDesenvolvedor::instanciar();
    cntrIUDesenvolvedorB = CntrIUDesenvolvedor::instanciar();

    // Demonstra que ambas vari�vies referenciam o mesmo objeto.

    if(cntrIUDesenvolvedorA == cntrIUDesenvolvedorB)
        cout << "Mesma instancia da controladora." << endl;
    else
        cout << "Diferentes instancias da controladora." << endl;


// EXEMPLO
// --------------------------------------------------------

    // Ilustra uso do stub de persist�ncia e a capacidade do c�digo exemplo distinguir comandos.

    cout << "Ilustra capacidade do stub de persist�ncia identificar os tipos de comandos." << endl;

    IPersistencia *cntrPersistencia = new StubPersistencia();

    // Cria comandos e invoca o stub.

    ComandoBancoDados *comandoBancoDados;

    comandoBancoDados = new ComandoIncluirGerente();

    cntrPersistencia->executar(comandoBancoDados);

    delete comandoBancoDados;

    comandoBancoDados = new ComandoRemoverGerente();

    cntrPersistencia->executar(comandoBancoDados);

    delete comandoBancoDados;

    comandoBancoDados = new ComandoEditarGerente();

    cntrPersistencia->executar(comandoBancoDados);

    delete comandoBancoDados;

    comandoBancoDados = new ComandoRecuperarGerente();

    cntrPersistencia->executar(comandoBancoDados);

    delete comandoBancoDados;


    return 0;
}
