#include "controladoras.h"

// Aloca��o e inicializa��es de vari�veis est�ticas.

CntrIUDesenvolvedor * CntrIUDesenvolvedor::referencia = 0;

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todos da classe CntrIUAutenticacao.

ResultadoAutenticacao CntrIUAutenticacao::autenticar() throw(runtime_error) {

    ResultadoAutenticacao resultado;
    Matricula matricula;
    Senha senha;
    int entrada;

    // Solicitar matricula e senha.

    while(true) {

        cout << endl << "Autenticacao de usuario." << endl << endl;

        try {
            cout << "Digite a matricula : ";
            cin >> entrada;
            matricula.setValor(entrada);
            cout << "Digite a senha     : ";
            cin >> entrada;
            senha.setValor(entrada);
            break;
        }
        catch (const invalid_argument &exp) {               // catch por refer�ncia.
            cout << endl << "Dado em formato incorreto." << endl;
        }
    }

    // Solicitar autentica��o.

    resultado = cntrLNAutenticacao->autenticar(matricula, senha);

    // Informar resultado da autentica��o.

    if(resultado.getValor() == ResultadoAutenticacao::FALHA)
    cout << endl << "Falha na autenticacao." << endl;

    return resultado;
}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todos da classe CntrIUProjeto.

void CntrIUProjeto::executar(const Matricula &matricula) throw(runtime_error){

    int opcao;

    while(true){

        // Ilustra limpeza de tela.

        // system("CLS");

        // Apresentar as op��es.

        cout << endl << "Gerenciamento de projeto." << endl << endl;

        cout << "Incluir  - " << INCLUIR << endl;
        cout << "Remover  - " << REMOVER << endl;
        cout << "Pesquiar - " << PESQUISAR << endl;
        cout << "Editar   - " << EDITAR << endl;
        cout << "Retornar - " << RETORNAR << endl << endl;
        cout << "Selecione uma opcao :";

        cin >> opcao;

        switch(opcao){
            case INCLUIR:   incluir();
                            break;
            case REMOVER:   remover();
                            break;
            case PESQUISAR: pesquisar();
                            break;
            case EDITAR:    editar();
                            break;
        }

        if(opcao == RETORNAR)
            break;
    }

    return;
}

void CntrIUProjeto::incluir() throw(runtime_error){

    Projeto projeto;

    // Solicitar dados do projeto a ser inclu�do.

    // Solicitar servi�o.

    ResultadoProjeto resultado;
    resultado = cntrLNProjeto->incluir(projeto);

    // Processar resultado.

}

void CntrIUProjeto::remover() throw(runtime_error){

    Codigo codigo;

    // Solicitar c�digo do projeto a ser removido.

    // Solicitar servi�o.

    ResultadoProjeto resultado;
    resultado = cntrLNProjeto->remover(codigo);

    // Processar resultado.

}

void CntrIUProjeto::pesquisar() throw(runtime_error){

    Codigo codigo;

    // Solicitar c�digo do projeto a ser pesquisado.


    // Solicitar servi�o.

    ResultadoProjeto resultado;
    resultado = cntrLNProjeto->pesquisar(codigo);

    // Processar resultado.

}

void CntrIUProjeto::editar() throw(runtime_error){

    Projeto projeto;

    // Solicitar dados do projeto a ser editado.

    // Solicitar servi�o.

    ResultadoProjeto resultado;
    resultado = cntrLNProjeto->editar(projeto);

    // Processar resultado.

}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todos da classe CntrIUGerente.

void CntrIUGerente::executar(const Matricula &matricula) throw(runtime_error){

    int opcao;

    while(true){

        // Ilustra limpeza de tela.

        // system("CLS");

        // Apresentar as op��es.

        cout << endl << "Gerenciamento de Gerente." << endl << endl;

        cout << "Incluir  - " << INCLUIR << endl;
        cout << "Remover  - " << REMOVER << endl;
        cout << "Pesquiar - " << PESQUISAR << endl;
        cout << "Editar   - " << EDITAR << endl;
        cout << "Retornar - " << RETORNAR << endl << endl;
        cout << "Selecione uma opcao :";

        cin >> opcao;

        switch(opcao){
            case INCLUIR:   incluir();
                            break;
            case REMOVER:   remover();
                            break;
            case PESQUISAR: pesquisar();
                            break;
            case EDITAR:    editar();
                            break;
        }

        if(opcao == RETORNAR)
            break;
    }

    return;
}

void CntrIUGerente::incluir() throw(runtime_error){
    CntrInclusao *cntr = new CntrInclusao();
    cntr->executar(cntrLNGerente);
    delete cntr;
}

void CntrIUGerente::remover() throw(runtime_error){
    CntrRemocao *cntr = new CntrRemocao();
    cntr->executar(cntrLNGerente);
    delete cntr;
}

void CntrIUGerente::pesquisar() throw(runtime_error){
    CntrPesquisa *cntr = new CntrPesquisa();
    cntr->executar(cntrLNGerente);
    delete cntr;
}

void CntrIUGerente::editar() throw(runtime_error){
    CntrEdicao *cntr = new CntrEdicao();
    cntr->executar(cntrLNGerente);
    delete cntr;
}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todos das classes internas de CntrIUGerente.

void CntrIUGerente::CntrEdicao::executar(ILNGerente *cntrLNGerente) throw(runtime_error) {

    Gerente gerente;

    // Solicitar dados do gerente a ser editado.

    // . . . .

    // Solicitar servi�o.

    ResultadoGerente resultado;
    resultado = cntrLNGerente->editar(gerente);

    // Processar resultado.
}

void CntrIUGerente::CntrInclusao::executar(ILNGerente *cntrLNGerente) throw(runtime_error) {

    Gerente gerente;

    // Solicitar dados do gerente a ser inclu�do.

    // . . . .

    // Solicitar servi�o.

    ResultadoGerente resultado;
    resultado = cntrLNGerente->incluir(gerente);

    // Processar resultado.

}

void CntrIUGerente::CntrPesquisa::executar(ILNGerente *cntrLNGerente) throw(runtime_error) {

    Matricula matricula;

    // Solicitar dados do gerente a ser pesquisado.

    // . . . .

    // Solicitar servi�o.

    ResultadoGerente resultado;
    resultado = cntrLNGerente->pesquisar(matricula);

    // Processar resultado.

}

void CntrIUGerente::CntrRemocao::executar(ILNGerente *cntrLNGerente) throw(runtime_error) {

    Matricula matricula;

    // Solicitar dados do gerente a ser removido.

    // . . . .

    // Solicitar servi�o.

    ResultadoGerente resultado;
    resultado = cntrLNGerente->remover(matricula);

    // Processar resultado.

}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todos da classe CntrIUDesenvolvedor.

// M�todos sem implementa��es pois o intuito da classe � apenas ilustrar o padr�o Singleton.

// Em uma implementa��o real, o m�todo precisaria estar codificados.

void CntrIUDesenvolvedor::executar(const Matricula &matricula) throw(runtime_error){

}

void CntrIUDesenvolvedor::setCntrLNDesenvolvedor(ILNDesenvolvedor *cntrLNDesenvolvedor) {
    this->cntrLNDesenvolvedor = cntrLNDesenvolvedor;
}
