#ifndef DOMINIOS_H_INCLUDED
#define DOMINIOS_H_INCLUDED

#include <stdexcept>

using namespace std;

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe Codigo.

class Codigo {
private:
        int codigo;

        const static int LIMITE = 25;

        // Ilustra m�todo de valida��o.

        void validar(int) throw (invalid_argument);

public:
        void setCodigo(int) throw (invalid_argument);

        // Ilustra m�todo inline.

        int getCodigo(){
            return codigo;
        }
};

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe Matricula.

class Matricula {

private:

    int valor;
    void validar(int) throw (invalid_argument);

public:

    const static int MATRICULA_INVALIDA = 12345;

    void setValor(int) throw (invalid_argument);

    int getValor() const {
        return valor;
    }
};

// EXEMPLO
// --------------------------------------------------------

// Declara��o de classe Senha.

class Senha {

private:

    int valor;
    void validar(int) throw (invalid_argument);

public:

    const static int SENHA_INVALIDA = 12345;

    void setValor(int) throw (invalid_argument);

    int getValor() const {
        return valor;
    }
};

class Fase {

};

#endif // DOMINIOS_H_INCLUDED
