#ifndef ENTIDADES_H_INCLUDED
#define ENTIDADES_H_INCLUDED

#include "dominios.h"
#include <string>

using namespace std;

// EXEMPLO
// --------------------------------------------------------

// Relacionamento entre objetos de diferentes classes.


class Desenvolvedor {
    private:
        int matricula;
        string nome;
    public:

        void setMatricula(int matricula) {
            this->matricula = matricula;
        }

        int getMatricula() const {
            return matricula;
        }

        void setNome(string nome) {
            this->nome = nome;
        }

        string getNome() const {
            return nome;
        }
};

// EXEMPLO
// --------------------------------------------------------

class Projeto {
private:
    Codigo codigo;

public:

    void setCodigo(const Codigo &codigo){
        this->codigo = codigo;
    }

    Codigo getCodigo() const{
        return codigo;
    }
};

// EXEMPLO
// --------------------------------------------------------

// Declaração de classe Gerente.

class Gerente {

private:

    Matricula matricula;

public:

    void setMatricula(const Matricula&){
        this->matricula = matricula;
    }

    Matricula getMatricula() const {
        return matricula;
    }
};

// EXEMPLO
// --------------------------------------------------------

// Declarações de classes que modelam resultados.


class Resultado {

protected:
    int valor;

public:

    // Declarações de possíveis resultados.

    const static int SUCESSO = 0;
    const static int FALHA   = 1;

    void setValor(int valor){
        this->valor = valor;
    }

    int getValor() const {
        return valor;
    }
};

class ResultadoAutenticacao:public Resultado {

private:
    Matricula matricula;

public:
    void setMatricula(const Matricula &matricula){      // passagem por referência.
        this->matricula = matricula;
    }

    Matricula getMatricula() const {
        return matricula;
    }
};

class ResultadoProjeto:public Resultado {

private:
    Projeto projeto;

public:
    void setProjeto(const Projeto &projeto){      // passagem por referência.
        this->projeto = projeto;
    }

    Projeto getProjeto() const {
        return projeto;
    }
};

class ResultadoGerente:public Resultado {

private:
    Gerente gerente;

public:
    void setGerente(const Gerente &gerente){      // passagem por referência.
        this->gerente = gerente;
    }

    Gerente getGerente() const {
        return gerente;
    }
};

#endif // ENTIDADES_H_INCLUDED
