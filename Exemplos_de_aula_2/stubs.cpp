#include "stubs.h"

//Defini��es de constantes.

const int StubLNAutenticacao::TRIGGER_FALHA;
const int StubLNAutenticacao::TRIGGER_ERRO_SISTEMA;

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todo da classe stub do controlador da l�gica de neg�cio de autentica��o.

ResultadoAutenticacao StubLNAutenticacao::autenticar(const Matricula &matricula, const Senha &senha) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNAutenticacao::autenticar" << endl ;

    cout << "Matricula = " << matricula.getValor() << endl ;
    cout << "Senha     = " << senha.getValor()     << endl ;

    ResultadoAutenticacao resultado;

    // Diferentes comportamentos dependendo do valor da matr�cula.

    switch(matricula.getValor()){
        case TRIGGER_FALHA:
            resultado.setValor(ResultadoAutenticacao::FALHA);
            break;
        case TRIGGER_ERRO_SISTEMA:
            throw runtime_error("Erro de sistema");
        default:
            resultado.setValor(ResultadoAutenticacao::SUCESSO);
            resultado.setMatricula(matricula);
        }

    return resultado;
}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todo da classe stub da controladora da l�gica de neg�cio de projeto.

ResultadoProjeto StubLNProjeto::incluir(const Projeto &projeto) throw(runtime_error){

    // Apresentar dados recebidos.

    cout << endl << "StubLNProjeto::incluir" << endl ;

    ResultadoProjeto resultado;

    // Diferentes comportamentos dependendo do c�digo do projeto.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

ResultadoProjeto StubLNProjeto::remover(const Codigo &codigo) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNProjeto::remover" << endl ;

    ResultadoProjeto resultado;

    // Diferentes comportamentos dependendo do c�digo do projeto.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

ResultadoProjeto StubLNProjeto::pesquisar(const Codigo &codigo) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNProjeto::pesquisar" << endl ;

    ResultadoProjeto resultado;

    // Diferentes comportamentos dependendo do c�digo do projeto.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

ResultadoProjeto StubLNProjeto::editar(const Projeto &projeto) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNProjeto::editar" << endl ;

    ResultadoProjeto resultado;

    // Diferentes comportamentos dependendo do c�digo do projeto.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todo da classe stub da controladora da l�gica de neg�cio de gerente.

ResultadoGerente StubLNGerente::incluir(const Gerente &gerente) throw(runtime_error){

    // Apresentar dados recebidos.

    cout << endl << "StubLNGerente::incluir" << endl ;

    ResultadoGerente resultado;

    // Diferentes comportamentos dependendo da matr�cula do gerente.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

ResultadoGerente StubLNGerente::remover(const Matricula &matricula) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNGerente::remover" << endl ;

    ResultadoGerente resultado;

    // Diferentes comportamentos dependendo da matr�cula do gerente.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

ResultadoGerente StubLNGerente::pesquisar(const Matricula &matricula) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNGerente::pesquisar" << endl ;

    ResultadoGerente resultado;

    // Diferentes comportamentos dependendo da matr�cula do gerente.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

ResultadoGerente StubLNGerente::editar(const Gerente &gerente) throw(runtime_error) {

    // Apresentar dados recebidos.

    cout << endl << "StubLNGerente::editar" << endl ;

    ResultadoGerente resultado;

    // Diferentes comportamentos dependendo do c�digo do projeto.

    // Sucesso.

    // Falha.

    // Erro de sistema.

    return resultado;
}

// EXEMPLO
// --------------------------------------------------------

// Defini��es de m�todo da classe stub da controladora de persist�ncia.

void StubPersistencia::executar (const ComandoBancoDados* comandoBancoDados) throw(runtime_error){

    // Ilustra como identificar a classe do comando recebido.

    if (typeid(*comandoBancoDados) == typeid(ComandoIncluirGerente)){
            cout << "Instancia da classe ComandoIncluirGerente" << endl;
    }

    if (typeid(*comandoBancoDados) == typeid(ComandoRemoverGerente)){
            cout << "Instancia da classe ComandoRemoverGerente" << endl;
    }

    if (typeid(*comandoBancoDados) == typeid(ComandoEditarGerente)){
            cout << "Instancia da classe ComandoEditarGerente" << endl;
    }

    if (typeid(*comandoBancoDados) == typeid(ComandoRecuperarGerente)){
            cout << "Instancia da classe ComandoRecuperarGerente" << endl;
    }
}
