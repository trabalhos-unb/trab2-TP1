#ifndef INTERFACE_APRESENTACAO_H_INCLUDED
#define INTERFACE_APRESENTACAO_H_INCLUDED

#include "./Interface_Objetos.h"
#include "./Interface_Negocio.h"
#include "../../include/Dominios.h"

/**
 * \defgroup Autenticacao
 * @{
 */
/**@}*/

using namespace std;

/**
\brief Define interface para camada de apresentacao de autenticacao
\ingroup Autenticacao
*/
class IMAAutenticacao{
    public:
        ///Declara o método padrão para Autenticar
        virtual ResultadoAutenticacao autenticar() = 0;
        ///Declara o método para estabelecer link entre camada de negócio e de apresentação
        virtual void setCntrAutenticacao(IMNAutenticacao *) = 0;
};

/**
 * \defgroup Pessoas
 * @{
 */
/**@}*/

/**
\brief Define interface base para camada de apresentacao das Entidades
\ingroup Pessoas
*/
class IMAEmpregados{
    public:
        static const int GERENTE_PROJETO = 2;
        static const int GERENTE_SISTEMAS = 3;
        static const int DESENVOLVEDOR = 1;
        ///Declara metodo padrão para exibir informações pessoais
        virtual void exibirInformacoesPessoais() = 0;
        ///Declara metodo padrão para ediçao das informaçoes pessoais
        virtual void editarInformacoesPessoais() = 0;
        ///Declara metodo padrão de exibição do menu de escolhas de funcionalidades
        virtual void menuPrincipal() = 0;
};

/**
\brief Define interface para camada de apresentacao de Desenvolvedor
\ingroup Pessoas
*/
class IMADesenvolvedor:public IMAEmpregados{
    public:
        ///Declara metodo padrão para editar informações de Projetos
        virtual void editarInformacoesProjetos() = 0;
        ///Declara o método para estabelecer link entre camada de negócio e de apresentação
        virtual void setCntrDesenvolvedor(IMNDesenvolvedor*) = 0;
};

/**
 * \defgroup Projeto
 * @{
 */
/**@}*/

/**
\brief Define interface base para camada de apresentacao dos Projetos
\ingroup Projeto
*/

class IMAProjeto{
    public:
        ///Declara metodo para editar, incluir, remover e pesquisar Projetos
        virtual void executar(const Matricula&) = 0;
        ///Declara o método para estabelecer link entre camada de negócio e de apresentação
        virtual void setCntrProjeto (IMNProjeto *) = 0;
};

#endif // INTERFACE_APRESENTACAO_H_INCLUDED
