#ifndef INTERFACE_OBJETOS_H_INCLUDED
#define INTERFACE_OBJETOS_H_INCLUDED

#include "../include/Entidades.h"

class Resultado{
    public:
        const static int FALHA = -1;
        const static int SUCESSO = 0;

        void setResultado(int novoEstado){
            estado = novoEstado;
        };
        int getResultado() const{
            return estado;
        };
    private:
        int estado;
};

class ResultadoAutenticacao:public Resultado{
    public:
        int getPerfil() const{
            return perfil;
        };
        void setPerfil(int novoPerfil){
            perfil = novoPerfil;
        };
        Desenvolvedor* getDesenvolvedor(){
            return desenvolvedor;
        }
        GerenteDeProjeto* getGerenteProjeto(){
            return gerenteProjeto;
        }
        GerenteDeSistema* getGerenteSistema(){
            return gerenteSistema;
        }
        void setDesenvolvedor(Desenvolvedor* d){
            desenvolvedor = d;
        }
        void setGerenteProjeto(GerenteDeProjeto* gp){
            gerenteProjeto = gp;
        }
        void setGerenteSistema(GerenteDeSistema* gs){
            gerenteSistema = gs;
        }
    private:
        int perfil;
        Desenvolvedor* desenvolvedor;
        GerenteDeProjeto* gerenteProjeto;
        GerenteDeSistema* gerenteSistema;
};

class ResultadoProjeto{
     public:
        const static int FALHA = -1;
        const static int SUCESSO = 0;

        void setResultado(int novoEstado){
            estado = novoEstado;
        };
        int getResultado() const{
            return estado;
        };
    private:
        int estado;


};

#endif // INTERFACE_OBJETOS_H_INCLUDED
