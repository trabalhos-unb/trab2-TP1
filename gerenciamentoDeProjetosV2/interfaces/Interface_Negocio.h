#ifndef INTERFACE_NEGOCIO_H_INCLUDED
#define INTERFACE_NEGOCIO_H_INCLUDED

#include "../include/Dominios.h"
#include "../include/Entidades.h"
#include "./Interface_Objetos.h"

/**
\brief Define interface para camada de negocio de Autenticacao
\ingroup Autenticacao
*/
class IMNAutenticacao{
    public:
        ///Declara metodo padrão para Autenticacao na camada de neǵocio
        virtual ResultadoAutenticacao autenticar(const Matricula &, const Senha &) = 0;
};

/**
\brief Define interface para camada de negócio de Desenvolvedor
\ingroup Pessoas
*/
class IMNDesenvolvedor{
    public:
        ///Declara método padrão para edição de informações de projetos na camada de negócio
        virtual void editarInformacoesProjetos(const Matricula&, const Custo&) = 0;
        ///Declara método padrão para edição de informações pessoais na camada de negócio
        virtual void editarInformacoesPessoais(const Nome&, const Senha&, const Email&) = 0;
};

/**
\brief Classe que modela a interface do modulo de negocio de Projeto
\ingroup Projeto
*/

class IMNProjeto{
    public:
        ///Declara método padrão para inclusao de projeto na camada de negócio
        virtual ResultadoProjeto incluir(const Projeto&) throw(runtime_error) = 0;
        ///Declara método padrão para remocao de um determinado projeto na camada de negócio
        virtual ResultadoProjeto remover(const CodigoProjeto&) throw(runtime_error) = 0;
        ///Declara método padrão para pesquisa de um determinado projeto na camada de negócio
        virtual ResultadoProjeto pesquisar(const CodigoProjeto&) throw(runtime_error) = 0;
        ///Declara método padrão para edicao das informacoes de um determinado projeto na camada de negócio
        virtual ResultadoProjeto editar(const Projeto&) throw(runtime_error) = 0;
};

#endif // INTERFACE_NEGOCIO_H_INCLUDED
