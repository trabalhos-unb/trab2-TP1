#ifndef STUBMNCNTRPROJETO_H
#define STUBMNCNTRPROJETO_H

#include "../../include/Dominios.h"
#include "../../include/Entidades.h"
#include "../../interfaces/Interface_Negocio.h"

/**
\brief Classe que simula interação com a camada de negócio de Projeto
\ingroup Projeto
*/

class StubMNCntrProjeto:public IMNProjeto{
    public:
        /**Simula a ação de incluir,
        * Levar em consideracao que o custo atual
        * nao pode ser superior ao custo previsto.
        */
        ResultadoProjeto incluir(const Projeto&) throw(runtime_error);

        /**Simula a ação de remover,
        * onde eh preciso um codigo de projeto
        * para remover um projeto:
        * - ABCDE - Codigo
        * Qualquer outro valor, nao vai ser
        * possivel remover o projeto, pois o
        * projeto nao existe.
        */
        ResultadoProjeto remover(const CodigoProjeto&) throw(runtime_error);

        /** Simula a acao de pesquisar dado um
        * determinado codigo:
        * - ABCDE - Codigo
        * Qualquer outro valor nao sera possivel
        * pesquisar pelo projeto.
        */
        ResultadoProjeto pesquisar(const CodigoProjeto&) throw(runtime_error);

        /**Simula a acao de editar um projeto
        * Levar em consideracao que o custo atual
        * nao pode ser superior ao custo previsto.
        */
        ResultadoProjeto editar(const Projeto&) throw(runtime_error);

};


#endif // STUBMNCNTRPROJETO_H
