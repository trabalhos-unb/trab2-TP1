#include "StubMNCntrProjeto.h"

ResultadoProjeto StubMNCntrProjeto::incluir(const Projeto &projeto) throw (runtime_error){
    ResultadoProjeto resultado;
    if(projeto.getCustoAtual() > projeto.getCustoPrevisto()){
        resultado.setResultado(ResultadoProjeto::FALHA);
        cout << "Custo atual não pode ser superior ao custo previsto." << endl;
    }
    else{
        resultado.setResultado(ResultadoProjeto::SUCESSO);
    }

    return resultado;
}

ResultadoProjeto StubMNCntrProjeto::editar(const Projeto &projeto) throw (runtime_error){
    ResultadoProjeto resultado;
    string continuar;

    cout << "Nome: " << projeto.getNome() <<endl;
    cout << "Data de inicio: " << projeto.getDataInicio() << endl;
    cout << "Data de termino: " << projeto.getDataTermino() <<endl;
    cout << "Custo atual: " << projeto.getCustoAtual() <<endl;
    cout << "Custo previsto: " << projeto.getCustoPrevisto() <<endl;
    cout << "Estado: " << projeto.getEstado() <<endl;
    cout << "Gerente de projeto\n";
    cout << "Nome: " << endl;
    cout << "Matricula: " << endl;
    cout << "Telefone: " << endl;
    cout << "Confirmar edicoes? 1-Sim 2-Nao"<< endl;
    cin >> continuar;

    if(projeto.getCustoAtual() > projeto.getCustoPrevisto()){
        resultado.setResultado(ResultadoProjeto::FALHA);
        cout << "Custo atual não pode ser superior ao custo previsto." << endl;
    }
    else{
        resultado.setResultado(ResultadoProjeto::SUCESSO);
    }

    return resultado;
}

ResultadoProjeto StubMNCntrProjeto::pesquisar(const CodigoProjeto &codigo) throw (runtime_error){
    ResultadoProjeto resultado;
    string cod("ABCDE");
    string continuar;

    int compare = cod.compare(codigo.getCodigoProjeto());

    if(compare == 0){
        resultado.setResultado(ResultadoProjeto::SUCESSO);
        cout << "Nome: SGPROJETO" << endl;
        cout << "Data de inicio: 12/12/2016" << endl;
        cout << "Data de termino: 12/12/2017" << endl;
        cout << "Custo atual: 12000.0" << endl;
        cout << "Custo previsto: 20000.0" << endl;
        cout << "Estado: Ativo" << endl;
        cout << "Gerente de projeto\n";
        cout << "Nome: JOAO" << endl;
        cout << "Matricula: 12345" << endl;
        cout << "Telefone: 99996666\n" << endl;
        cout << "Digite 1 para continuar\n";
        cin >> continuar;
    }
    else{
        resultado.setResultado(ResultadoProjeto::FALHA);
        cout << "O codigo digitado nao existe\n";
    }



    return resultado;
}

ResultadoProjeto StubMNCntrProjeto::remover(const CodigoProjeto &codigo) throw (runtime_error){
    ResultadoProjeto resultado;
    string cod("ABCDE");
    int opcao;
    string continuar;
    int compare = cod.compare(codigo.getCodigoProjeto());

    if(compare == 0){
        resultado.setResultado(ResultadoProjeto::SUCESSO);
        cout << "Nome: SGPROJETO" << endl;
        cout << "Data de inicio: 12/12/2016" << endl;
        cout << "Data de termino: 12/12/2017" << endl;
        cout << "Custo atual: 12000.0" << endl;
        cout << "Custo previsto: 20000.0" << endl;
        cout << "Estado: Ativo" << endl;
        cout << "Gerente de projeto\n";
        cout << "Nome: JOAO" << endl;
        cout << "Matricula: 12345" << endl;
        cout << "Telefone: 99996666\n" << endl;
        cout << "Remover o projeto? 1-Sim 2-Nao\n";
        cin >> opcao;
        if(opcao == 1){
            cout << "Projeto removido. Digite 1 para continuar.\n";
            cin >> continuar;
        }

    }
    else{
        resultado.setResultado(ResultadoProjeto::FALHA);
        cout << "O codigo digitado nao existe\n";
    }


    return resultado;
}



