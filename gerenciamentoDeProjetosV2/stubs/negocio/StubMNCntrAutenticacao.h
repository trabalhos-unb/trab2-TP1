#ifndef STUB_MNCNTRAUTENTICACAO_H
#define STUB_MNCNTRAUTENTICACAO_H

#include "../../include/Dominios.h"
#include "../../interfaces/Interface_Apresentacao.h"
#include "../../interfaces/Interface_Negocio.h"

/**
\brief Classe que simula interação com a camada de negócio da Autenticacao
\ingroup Autenticacao
*/
class StubMNCntrAutenticacao:public IMNAutenticacao{
    public:
        /**Simulada ação de autenticar,
        * onde são validas apenas três matrículas:
        * - 12345 - Desenvolvedor
        * - 23456 - Gerente de Sistema
        * - 34567 - Gerente de Projeto
        *\n Qualquer valor de senha é válido
        */
        ResultadoAutenticacao autenticar(const Matricula &,const Senha &);
};

#endif // STUB_MNCNTRAUTENTICACAO_H
