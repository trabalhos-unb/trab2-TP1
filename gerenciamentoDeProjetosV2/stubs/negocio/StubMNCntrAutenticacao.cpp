#include "StubMNCntrAutenticacao.h"

ResultadoAutenticacao StubMNCntrAutenticacao::autenticar(const Matricula &matricula, const Senha &senha){
    const string m_desenvolvedor("12345");
    const string m_gerenteSistema("23456");
    const string m_gerenteProjeto("34567");

    Desenvolvedor* d = new Desenvolvedor();
    GerenteDeProjeto* gp = new GerenteDeProjeto();
    GerenteDeSistema* gs = new GerenteDeSistema();

    ResultadoAutenticacao resultado;

    resultado.setResultado( ResultadoAutenticacao::FALHA);

    if( !m_desenvolvedor.compare(matricula.getMatricula()) ){

        resultado.setResultado( ResultadoAutenticacao::SUCESSO);
        resultado.setPerfil( IMAEmpregados::DESENVOLVEDOR );
        resultado.setDesenvolvedor(d);

    }else if( !m_gerenteSistema.compare(matricula.getMatricula()) ){

        resultado.setResultado( ResultadoAutenticacao::SUCESSO);
        resultado.setPerfil( IMAEmpregados::GERENTE_SISTEMAS );

        resultado.setGerenteSistema(gs);
    }else if( !m_gerenteProjeto.compare(matricula.getMatricula()) ){

        resultado.setResultado( ResultadoAutenticacao::SUCESSO);
        resultado.setPerfil( IMAEmpregados::GERENTE_PROJETO );
        resultado.setGerenteProjeto(gp);

    }

    return resultado;
};
