#ifndef STUB_MNCNTRDESENVOLVEDOR_H
#define STUB_MNCNTRDESENVOLVEDOR_H

#include <iostream>

#include "../../include/Dominios.h"
#include "../../interfaces/Interface_Negocio.h"

using namespace std;

/**
\brief Classe que simula interação com a camada de Negócio de Desenvolvedor
\ingroup Pessoas
*/
class StubMNCntrDesenvolvedor:public IMNDesenvolvedor{
    public:
        void editarInformacoesProjetos(const Matricula&, const Custo&);
        void editarInformacoesPessoais(const Nome&, const Senha&, const Email&);
};

#endif // STUB_MNCNTRDESENVOLVEDOR_H
