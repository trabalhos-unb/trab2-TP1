#ifndef DOMINIOS_H_INCLUDED
#define DOMINIOS_H_INCLUDED

#include "./Dominios/Nome_Dominio.h"
#include "./Dominios/Telefone_Dominio.h"
#include "./Dominios/Matricula_Dominio.h"
#include "./Dominios/Email_Dominio.h"
#include "./Dominios/Senha_Dominio.h"
#include "./Dominios/Custo_Dominio.h"
#include "./Dominios/Codigo_Projeto.h"
#include "./Dominios/Estado_Projeto.h"
#include "./Dominios/Fase_Projeto.h"
#include "./Dominios/Funcao.h"
#include "./Dominios/Data.h"

#endif // DOMINIOS_H_INCLUDED
