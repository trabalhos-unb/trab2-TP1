#ifndef FUNCAO_H
#define FUNCAO_H

#include<iostream>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio Funcao
\ingroup Dominios
*/

class Funcao{

    private:
        int funcao;
        void validarFuncao(int) throw (invalid_argument);
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        Funcao();
        /** Construtor de Funcao,
        * recebe como argumento um inteiro(1(Analista), 2(Projetista) ou 3(Programador))
        *
        */
        Funcao(int);
         ///Permite atualizar o valor do objeto Funcao. Lança exceção em caso de argumento inválido
        void setFuncao(int);
         ///Permite  extrair o atual valor do objeto Funcao
        int getFuncao() const;
        const static int ANALISTA;
        const static int PROJETISTA;
        const static int PROGRAMADOR;

};

#endif // FUNCAO_H
