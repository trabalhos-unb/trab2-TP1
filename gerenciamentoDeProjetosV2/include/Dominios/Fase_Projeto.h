#ifndef FASE_PROJETO_H
#define FASE_PROJETO_H

#include <iostream>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio FaseProjeto
\ingroup Dominios
*/

class FaseProjeto
{
    private:
        int fase;
        void validarFase(int) throw(invalid_argument);
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        FaseProjeto();
        /** Construtor de FaseProjeto,
        * recebe como argumento um inteiro
        *
        */
        FaseProjeto(int);
        ///Destrutor padrao de objeto
        ~FaseProjeto();
        ///Permite  extrair o atual valor do objeto FaseProjeto
        int getFaseProjeto() const;
        ///Permite atualizar o valor do objeto FaseProjeto. Lança exceção em caso de argumento inválido
        void setFaseProjeto(int);

};

#endif // FASE_PROJETO_H
