#include "Funcao.h"
const int Funcao::ANALISTA = 1;
const int Funcao::PROJETISTA = 2;
const int Funcao::PROGRAMADOR = 3;

Funcao::Funcao(){
    funcao = PROGRAMADOR;
}

Funcao::Funcao(int funcao){
    validarFuncao(funcao);
    this->funcao = funcao;
}

int Funcao::getFuncao() const{
    return funcao;
}

void Funcao::setFuncao(int funcao){
    validarFuncao(funcao);
    this->funcao = funcao;
}

void Funcao::validarFuncao(int funcao) throw (invalid_argument){
    if((funcao != ANALISTA) && (funcao != PROJETISTA) && (funcao != PROGRAMADOR))
        throw invalid_argument("Funcao Invalida. Funcoes existentes: 1(ANALISTA), 2(PROJETISTA), 3(PROGRAMADOR)");
}
