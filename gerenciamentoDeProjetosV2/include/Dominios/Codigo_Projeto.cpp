#include "Codigo_Projeto.h"

const int CodigoProjeto::totalLetras = 5;

CodigoProjeto::CodigoProjeto(){
    codigo = "";
}
CodigoProjeto::CodigoProjeto(string codigo){
    validaCodigo(codigo);
    this->codigo = codigo;
}

CodigoProjeto::~CodigoProjeto(){
}

void CodigoProjeto::validaCodigo(string codigo) throw (invalid_argument){
    if(codigo.length() != totalLetras){
        throw invalid_argument("O codigo deve ter 5 letras.");
    }
    for(unsigned int indice = 0; indice < codigo.length(); indice++){
        if(isalpha(codigo[indice]) == 0 || isupper(codigo[indice]) == 0){
            throw invalid_argument ("Codigo com Formato errado. Formato correto: A-Z");
        }

    }
}

string CodigoProjeto::getCodigoProjeto() const {
    return codigo;
}

void CodigoProjeto::setCodigoProjeto(string codigo){
    validaCodigo(codigo);
    this->codigo = codigo;
}
