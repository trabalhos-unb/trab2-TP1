#include "Data.h"
const int Data::TOTAL_ELEMENTOS_DATA = 10;

Data::Data(){
    data = "01/01/2016";
}

Data::Data(string data){
    validarData(data);
    this->data = data;
}

string Data::getData() const{
    return data;
}

void Data::setData(string data){
    validarData(data);
    this->data = data;
}

void Data::validarData(string data) throw (invalid_argument){

    if(data[2] != '/' || data[5] != '/' || data.length() != TOTAL_ELEMENTOS_DATA)
        throw invalid_argument("Formato invalido de Data. Formato correto: DD/MM/AAAA");



    validarDia(data);
    validarMes(data);
    validarAno(data);
}

void Data::validarDia(string data) throw (invalid_argument){
    //junta em apenas uma string
    stringstream dd;
    dd << data[0] << data[1];
    string d = dd.str();
    //transforma a string em int e atribui ao dia;
    stringstream s(d);
    s >> dia;
    if(dia <= 0 || dia > 31)
        throw invalid_argument ("Dia invalido. O dia deve estar entre 1-31");
}
void Data::validarMes(string data) throw (invalid_argument){
    //junta em apenas uma string
    stringstream mm;
    mm << data[3] << data[4];
    string m = mm.str();
    //transforma a string em int e atribui ao mes;
    stringstream s(m);
    s >> mes;
    if(mes <= 0 || mes > 12)
        throw invalid_argument ("Mes invalido. O mes deve estar entre 1-12");
}
void Data::validarAno(string data) throw (invalid_argument){
    //junta em apenas uma string
    stringstream aa;
    aa << data[6] << data[7] << data[8] << data[9];
    string a = aa.str();
    //transforma a string em int e atribui ao ano;
    stringstream s(a);
    s >> ano;
    if(ano <= 2015 || ano > 2050)
        throw invalid_argument ("Ano invalido. O ano deve estar entre 2016-2050");
}
