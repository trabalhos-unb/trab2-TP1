#include "Senha_Dominio.h"

Senha::Senha(){
    senha = "";
};

Senha::Senha(string senha){
    validaSenha(senha);
    this->senha = senha;
};

Senha::~Senha(){
};

void Senha::validaSenha(string senha) throw(invalid_argument){
    string senha_ordenada = senha;
    if( !senha.compare("") ){
        throw invalid_argument("*Senha sem nenhum caracter");
    }
    if( senha.length() != maxCaracter ){
        throw invalid_argument("*Necessario 5 caracter");
    }
    /*procura por caracteres repetidos*/
    sort(senha_ordenada.begin(), senha_ordenada.end());
    for(unsigned int indice = 0 ; indice < senha_ordenada.length() ; indice++ ){
        if( senha_ordenada[indice] == senha_ordenada[indice + 1] ){
            throw invalid_argument("*Caracteres repetidos");
        }
    }
};

string Senha::getSenha() const{
    return senha;
};

void Senha::setSenha(string novoSenha){
    validaSenha(novoSenha);
    senha = novoSenha;
};
