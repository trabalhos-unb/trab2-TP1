#ifndef TELEFONE_DOMINIO_H_INCLUDED
#define TELEFONE_DOMINIO_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio Telefone
\ingroup Dominios
*/

class Telefone{
    private:
        const static unsigned int maxCaracter = 8;
        string telefone;
        void validaTelefone(string) throw(invalid_argument);
    public:
        Telefone();
        /**Construtor de Telefone,
        * recebe uma string de 8 digitos de 0-9
        */
        Telefone(string);
        ~Telefone();
        ///Permite extrair o atual valor de Telefone
        string getTelefone() const;
        ///Permite atualizar o valor de Telefone. Lança exceção em caso de argumento inválido.
        void setTelefone(string);
};

#endif // TELEFONE_DOMINIO_H_INCLUDED
