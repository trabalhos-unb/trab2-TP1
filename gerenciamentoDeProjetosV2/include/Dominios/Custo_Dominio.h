#ifndef CUSTO_DOMINIO_H_INCLUDED
#define CUSTO_DOMINIO_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

/**
 * \defgroup Dominios
 * @{
 */
/**
*@}
*/

/**
\brief Classe que modela o dominio Custo
\ingroup Dominios
*/

class Custo{
    private:
        const static float custoMinimo = 0.0;
        float custo;

        void validaCusto(float) throw(invalid_argument);
    public:
        ///Construtor padrão, instacia o objeto com valores padrão
        Custo();
        /** Construtor de Custo
        * recebe um valor do tipo float maior que zero.
        * Lança exceção em caso de argumento inválido
        */
        Custo(float);
        ///Destrutor padrão do objeto
        ~Custo();
        ///Permite extrair o valor de Custo
        float getCusto() const;
        ///Permite atualizar o valor de Custo, lança exceção em caso de argumento inválido
        void setCusto(float);
};

#endif // CUSTO_DOMINIO_H_INCLUDED
