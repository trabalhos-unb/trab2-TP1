#ifndef DOMINIO_NOME_H_INCLUDED
#define DOMINIO_NOME_H_INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio Nome
\ingroup Dominios
*/

class Nome{
    private:
        const static unsigned int max_caracter = 20;
        string nome;
        void validaNome(string) throw(invalid_argument);
    public:
        ///Construtor padrão, instacia objeto com valor padrão
        Nome();
        /**Construtor de Matricula,
        * pode receber como argumento um string
        * de 20 caracters maiúsculos de A-Z.
        */
        Nome(string);
        ///Destrutor padrão do objeto
        ~Nome();
        ///Permite extrair o atual valor do objeto Nome.
        string getNome() const;
         ///Permite atualizar valor do objeto Nome. Lança exceção em caso de argumento inválido
        void setNome(string) throw(invalid_argument);
};

#endif // DOMINIO_NOME_H_INCLUDED
