#ifndef EMAIL_DOMINIO_H
#define EMAIL_DOMINIO_H

#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio Email
\ingroup Dominios
*/

class Email{
    private:
        string email;
        void validaEmail(string email) throw(invalid_argument);
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        Email();
        /** Construtor de Email,
        * recebe como argumento uma string em formato RFC
        *
        */
        Email(string);
        ///Destrutor padrão do objeto
        ~Email();
        ///Permite  extrair o atual valor do objeto Email
        string getEmail() const;
        ///Permite atualizar o valor do objeto Email. Lança exceção em caso de argumento inválido
        void setEmail(string);
};

#endif // EMAIL_DOMINIO_H
