#include "Fase_Projeto.h"


FaseProjeto::FaseProjeto()
{
   fase = 1;
}

FaseProjeto::FaseProjeto(int fase){
    validarFase(fase);
    this->fase = fase;
}

FaseProjeto::~FaseProjeto(){
}

int FaseProjeto::getFaseProjeto() const{
    return fase;
}


void FaseProjeto::validarFase(int fase) throw(invalid_argument){
    if((fase != 1) && (fase != 2) && (fase != 3) && (fase != 4))
        throw invalid_argument("Fase invalida. As fases validas sao: 1(Iniciacao), 2(Preparacao), 3(Exxecucao), 4(Encerramento). ");

}

void FaseProjeto::setFaseProjeto(int fase){
    validarFase(fase);
    this->fase = fase;
}
