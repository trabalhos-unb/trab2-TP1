#include "Estado_Projeto.h"

const int EstadoProjeto::ATIVO = 1;
const int EstadoProjeto::INATIVO = 2;

EstadoProjeto::EstadoProjeto()
{
    estado = INATIVO;
}
EstadoProjeto::EstadoProjeto(int estado){
    validarEstado(estado);
    this->estado = estado;
}

EstadoProjeto::~EstadoProjeto(){
}

int EstadoProjeto::getEstadoProjeto() const{
    return estado;
}

void EstadoProjeto::validarEstado(int estado) throw (invalid_argument){
    if(estado != ATIVO && estado != INATIVO)
        throw invalid_argument("O estado deve ser ATIVO(1) ou INATIVO(2)");
}
void EstadoProjeto::setEstadoProjeto(int estado){
    validarEstado(estado);
    this->estado = estado;
}
