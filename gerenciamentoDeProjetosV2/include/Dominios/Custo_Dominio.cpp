#include "Custo_Dominio.h"

Custo::Custo(){
    custo = 0.0;
};

Custo::Custo(float custo){
    validaCusto(custo);
    this->custo = custo;
};

Custo::~Custo(){
};

void Custo::validaCusto(float novoCusto) throw(invalid_argument){
    if( novoCusto < custoMinimo ){
        throw invalid_argument("*Valor menor que 0.0");
    }
};

float Custo::getCusto() const{
    return custo;
};

void Custo::setCusto(float novoCusto){
    validaCusto(novoCusto);
    custo = novoCusto;
};

