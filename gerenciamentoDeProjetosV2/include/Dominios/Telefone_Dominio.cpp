#include "Telefone_Dominio.h"

Telefone::Telefone(){
    telefone = "";
};

Telefone::Telefone(string telefone){
    validaTelefone(telefone);
    this->telefone = telefone;
};

Telefone::~Telefone(){
};

void Telefone::validaTelefone(string telefone) throw(invalid_argument){
    if( !telefone.compare("") ){
        throw invalid_argument("*Tente 8 digitos de 0-9");
    }
    if( telefone.length() != maxCaracter ){
        throw invalid_argument("*Sao necessarios 8 digitos");
    }
    /*Procura por não digitos de 0-9*/
    for(unsigned int indice = 0 ; indice < telefone.length() ; indice++){
        if( !std::isdigit(telefone[indice]) ){
            throw invalid_argument("*Caracteres nao digitos");
        }
    }

};

string Telefone::getTelefone() const{
    return telefone;
};

void Telefone::setTelefone(string novoTelefone){
    validaTelefone(novoTelefone);
    telefone = novoTelefone;
};
