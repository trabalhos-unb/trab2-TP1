#include "Matricula_Dominio.h"

Matricula::Matricula(){
    matricula = "";
};

Matricula::Matricula(string matricula){
    validaMatricula(matricula);
    this->matricula = matricula;
};

Matricula::~Matricula(){
};

void Matricula::validaMatricula(string novaMatricula) throw(invalid_argument){
    if( novaMatricula.length() != maxCaracter ){
        throw invalid_argument("*Sao necessarios 5 digitos");
    }
    /*Procura por digitos de 0-9 nao maiusculas*/
    for(unsigned int indice = 0 ; indice < novaMatricula.length() ; indice++){
        if( !std::isdigit(novaMatricula[indice]) ){
            throw invalid_argument("*Tente digitos de 0-9");
        }
    }
};

string Matricula::getMatricula() const{
    return matricula;
};

void Matricula::setMatricula(string novoMatricula){
    validaMatricula(novoMatricula);
    matricula = novoMatricula;
};

