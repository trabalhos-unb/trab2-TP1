#ifndef ESTADO_PROJETO_H
#define ESTADO_PROJETO_H

#include <iostream>
#include <stdexcept>

using namespace std;

/**
\brief Classe que modela o dominio EstadoProjeto
\ingroup Dominios
*/

class EstadoProjeto
{
    private:
        int estado;
        void validarEstado(int) throw(invalid_argument);
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        EstadoProjeto();
        /** Construtor de EstadoProjeto,
        * recebe como argumento um inteiro
        *
        */
        EstadoProjeto(int);
        ///Destrutor padrao de objeto
        ~EstadoProjeto();
        ///Permite  extrair o atual valor do objeto EstadoProjeto
        int getEstadoProjeto() const;
        ///Permite atualizar o valor do objeto EstadoProjeto. Lança exceção em caso de argumento inválido
        void setEstadoProjeto(int);
        const static int ATIVO;
        const static int INATIVO;
};

#endif // ESTADO_PROJETO_H
