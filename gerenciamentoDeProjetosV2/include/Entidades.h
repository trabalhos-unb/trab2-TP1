#ifndef ENTIDADES_H_INCLUDED
#define ENTIDADES_H_INCLUDED

#include "./Entidades/GerenteDeSistema_Entidade.h"
#include "./Entidades/GerenteDeProjeto_Entidade.h"
#include "./Entidades/Desenvolvedor_Entidade.h"
#include "./Entidades/Projeto.h"
#include "./Entidades/Fase_Entidade.h"

#endif // ENTIDADES_H_INCLUDED
