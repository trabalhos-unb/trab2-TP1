#ifndef DESENVOLVEDOR_ENTIDADE_H
#define DESENVOLVEDOR_ENTIDADE_H

#include <iostream>
#include <vector>

#include "../Dominios.h"
#include "../Entidades.h"

/**
 * \defgroup Entidades
 * @{
 */
/**@}*/


/**
\brief Classe que modela o dominio Desenvolvedor
\ingroup Entidades
*/

class Projeto;

class Desenvolvedor{
    private:
        Nome *nome;
        Matricula *matricula;
        Senha *senha;
        Email *email;
        Funcao *funcao;
        std::vector<Projeto *> projetos;
    public:
        ///Construtor padrão, instacia objeto com valores padrão
        Desenvolvedor();
        string getNome();
		string getMatricula();
		string getEmail();
		string getFuncao();
		void setNome(Nome *);
		void setSenha(Senha *);
		void setEmail(Email *);
        /** Construtor de Desenvolvedor
        * Recebe como parãmetros objetos válidos,
        * dos tipos:
        * - Ponteiro para objeto Nome
        * - Ponteiro para objeto Matricula
        * - Ponteiro para objeto Senha
        * - Ponteiro para objeto Email
        * - Ponteiro para objeto Funcao
        ** Não possui validação, dado que os objetos de dominio
        * possuem validação em seu instanciamento.
        */
        Desenvolvedor(Nome *, Matricula *, Senha *, Email *, Funcao *);
        /// Invoca o destrutor padrão de cada objeto atribuido e em seguida destroi o proprio objeto
        ~Desenvolvedor();
};

#endif // DESENVOLVEDOR_ENTIDADE_H
