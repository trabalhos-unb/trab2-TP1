#include "GerenteDeSistema_Entidade.h"

GerenteDeSistema::GerenteDeSistema(){
    this->nome = new Nome();
    this->matricula = new Matricula();
    this->senha = new Senha();
};

GerenteDeSistema::GerenteDeSistema(Nome *nome, Matricula *matricula, Senha *senha){
    this->nome = nome;
    this->matricula = matricula;
    this->senha = senha;
};

GerenteDeSistema::~GerenteDeSistema(){
    delete nome;
    delete matricula;
    delete senha;
};

string GerenteDeSistema::getNome() const{
    return nome->getNome();
};

string GerenteDeSistema::getMatricula() const{
    return matricula->getMatricula();
};
