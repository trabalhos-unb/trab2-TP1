#include "Fase_Entidade.h"

FaseEntidade::FaseEntidade(){
    this->dataInicio = new Data();
    this->dataTermino = new Data();
    this->codigoFase = new FaseProjeto();
}

FaseEntidade::FaseEntidade(Data * dataInicio, Data *dataTermino, FaseProjeto *codigoFase){
    this->dataInicio = dataInicio;
    this->dataTermino = dataTermino;
    this->codigoFase = codigoFase;
}

FaseEntidade::~FaseEntidade(){
    delete dataInicio;
    delete dataTermino;
    delete codigoFase;
}

string FaseEntidade::getDataInicio(){
    return dataInicio->getData();
}

string FaseEntidade::getDataTermino(){
    return dataTermino->getData();
}

int FaseEntidade::getFaseProjeto(){
    return codigoFase->getFaseProjeto();
}
