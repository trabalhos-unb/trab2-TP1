#include "Projeto.h"

Projeto::Projeto(){
    this->nome = new Nome();
    this->codigo = new CodigoProjeto();
    this->gerente = new GerenteDeProjeto();
    this->dataInicio = new Data();
    this->dataTermino = new Data();
    this->custoAtual = new Custo();
    this->custoPrevisto = new Custo();
    this->estado = new EstadoProjeto();
}

Projeto::Projeto(Nome *nome, CodigoProjeto *codigo, GerenteDeProjeto *gerente, Data *dataInicio, Data *dataTermino, Custo *custoAtual, Custo *custoPrevisto, EstadoProjeto *estado){
    this->nome = nome;
    this->codigo = codigo;
    this->gerente = gerente;
    this->dataInicio = dataInicio;
    this->dataTermino = dataTermino;
    this->custoAtual = custoAtual;
    this->custoPrevisto = custoPrevisto;
    this->estado = estado;
}

Projeto::~Projeto(){
    delete nome;
    delete codigo;
    delete gerente;
    delete dataInicio;
    delete dataTermino;
    delete custoAtual;
    delete custoPrevisto;
    delete estado;
}


void Projeto::addDesenvolvedor(Desenvolvedor *desenvolvedor){
    desenvolvedores.push_back(*desenvolvedor);
}

void Projeto::removerDesenvolvedores(){
    desenvolvedores.clear();
}

int Projeto::quantDesenvolvedores(){
    return desenvolvedores.size();
}

string Projeto::getNome() const{
    return nome->getNome();
}

void Projeto::setNome(string m_nome){
        nome->setNome(m_nome);
}

string Projeto::getCodigo() const{
    return codigo->getCodigoProjeto();
}

void Projeto::setCodigo(string m_codigo){
    codigo->setCodigoProjeto(m_codigo);
}

string Projeto::getDataInicio() const{
    return dataInicio->getData();
}

void Projeto::setDataInicio(string d_inicio){
    dataInicio->setData(d_inicio);
}

string Projeto::getDataTermino() const{
    return dataTermino->getData();
}

void Projeto::setDataTermino(string d_termino){
    dataTermino->setData(d_termino);
}

float Projeto::getCustoAtual() const{
    return custoAtual->getCusto();
}

void Projeto::setCustoAtual(float c_atual){
    custoAtual->setCusto(c_atual);
}

float Projeto::getCustoPrevisto() const{
    return custoPrevisto->getCusto();
}

void Projeto::setCustoPrevisto(float c_previsto){
    custoPrevisto->setCusto(c_previsto);
}

int Projeto::getEstado() const{
    return estado->getEstadoProjeto();
}

void Projeto::setEstado(int p_estado){
    estado->setEstadoProjeto(p_estado);
}





