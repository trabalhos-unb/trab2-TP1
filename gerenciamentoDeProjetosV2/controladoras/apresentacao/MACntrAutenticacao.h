#ifndef MA_CNTR_AUTENTICACAO_H
#define MA_CNTR_AUTENTICACAO_H

#include <iostream>
#include <string>
#include <stdexcept>

#include "../../interfaces/Interface_Apresentacao.h"
#include "../../include/Dominios.h"

using namespace std;

/**
\brief Classe que modela a controladora de Autenticação na camada de apresentação
\ingroup Autenticacao
*/
class MACntrAutenticacao:public IMAAutenticacao{
    public:
        /**Retorna objeto contendo resultado da autenticacao
        * e referencia para a respectiva entidade
        */
        ResultadoAutenticacao autenticar();
        ///Estabelecer relação entre controladora de autenticacao com a de negócio
        void setCntrAutenticacao(IMNAutenticacao *);
    private:
        IMNAutenticacao *MNCntrAutenticacao;
};

#endif // MA_CNTR_AUTENTICACAO_H
