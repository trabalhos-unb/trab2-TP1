#ifndef MACNTRGERENTEDESISTEMA_H
#define MACNTRGERENTEDESISTEMA_H

#include <iostream>
#include <stdexcept>

#include "../../include/Entidades.h"
#include "../../interfaces/Interface_Apresentacao.h"

using namespace std;

/**
\brief Classe que modela a controladora de GerenteDeSistema na camada de apresentação
\ingroup Pessoas
*/

class MACntrGerenteDeSistema:public IMAEmpregados{
    public:
        void exibirInformacoesPessoais();
        void editarInformacoesPessoais();
        void cadastrarGerenteProjeto();
        void descadastrarGerenteProjeto();
        void menuPrincipal();

        //Instancia apenas um Gerente no Sistema
        static MACntrGerenteDeSistema* instanciar(){
            if( referencia_cntr == 0 ){
                referencia_cntr = new MACntrGerenteDeSistema();
            }
            return referencia_cntr;
        };
    private:
        MACntrGerenteDeSistema();
        //referencia para instancia
        static MACntrGerenteDeSistema* referencia_cntr;
        static GerenteDeSistema* gerente_sistema;
};

#endif // MACNTRGERENTEDESISTEMA_H
