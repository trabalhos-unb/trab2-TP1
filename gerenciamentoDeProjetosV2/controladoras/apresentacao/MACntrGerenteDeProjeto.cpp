#include "MACntrGerenteDeProjeto.h"

MACntrGerenteDeProjeto* MACntrGerenteDeProjeto::referenciaCntr = 0;
GerenteDeProjeto* MACntrGerenteDeProjeto::gerenteProjeto = new GerenteDeProjeto();

MACntrGerenteDeProjeto::MACntrGerenteDeProjeto(){}

MACntrGerenteDeProjeto* MACntrGerenteDeProjeto::instanciar(){
    if( referenciaCntr == 0 ){
        referenciaCntr = new  MACntrGerenteDeProjeto();
    }
    return referenciaCntr;
}

void MACntrGerenteDeProjeto::menuPrincipal(){
    int opcao;
    const int SAIR = 8;
    do{
        cout << "1| Exibir informações pessoais" << endl;
        cout << "2| Editar informações pessoais" << endl;
        cout << "3| Cadastrar Projetos" << endl;
        cout << "4| Descadastrar Projetos" << endl;
        cout << "5| Cadastrar Desenvolvedores" << endl;
        cout << "6| Descadastrar Desenvolvedores" << endl;
        cout << "7| Associar Desenvolvedor a um Projeto" << endl;

        cout << "8| Sair do sistema" << endl;
        cout << ">> ";

        cin >> opcao;

        switch( opcao ){
            case 1:
                exibirInformacoesPessoais();
            break;
            case 2:
                editarInformacoesPessoais();
            break;
            case 3:
                cadastrarProjeto();
            break;
            case 4:
                descadastrarProjeto();
            break;
            case 5:
                cadastrarDesenvolvedor();
            break;
            case 6:
                descadastrarDesenvolvedor();
            break;
            case 7:
                associarDesenvolvedorComProjeto();
            break;
            case SAIR:
                //Sair
            break;
        };
    }while( opcao != SAIR );

}

void MACntrGerenteDeProjeto::exibirInformacoesPessoais(){
    cout << "Nome :" << endl;
    cout << "Matricula: " << endl;
}

void MACntrGerenteDeProjeto::editarInformacoesPessoais(){

}

void MACntrGerenteDeProjeto::cadastrarProjeto(){
    //Declaracao de variaveis
    string nome_s;
    string codigo_s;
    string data_inicio_s;
    string data_fim_s;
    string custo_atual_s;
    string custo_previsto_s;
    string estado_s;
    Nome nome;
    CodigoProjeto codigo;
    GerenteDeProjeto gerente;
    Data dataInicio;
    Data dataTermino;
    Custo custoAtual;
    Custo custoPrevisto;
    EstadoProjeto estado;

}

void MACntrGerenteDeProjeto::descadastrarProjeto(){
    string codigo_s;
    CodigoProjeto codigo;

    cout << "Entre com o Código de Projeto." << endl;
    cout << ">> ";
    cin >> codigo_s;

}
void MACntrGerenteDeProjeto::cadastrarDesenvolvedor(){

}

void MACntrGerenteDeProjeto::descadastrarDesenvolvedor(){
    string matricula_s;
    Matricula matricula;

    cout << "Entre com a Matrícula do Desenvolvedor." << endl;
    cout << ">> ";
    cin >> matricula_s;
}

void MACntrGerenteDeProjeto::associarDesenvolvedorComProjeto(){

}
