#include "MACntrAutenticacao.h"
#include "../../include/Dominios.h"

ResultadoAutenticacao MACntrAutenticacao::autenticar(){
    ResultadoAutenticacao resultado;
    Matricula m;
    string matricula;
    Senha s;
    string senha;

    while(true){
        try{
            cout << "Insira a Matricula : ";
            cin >> matricula;
            m.setMatricula(matricula);
            cout << "Insira a Senha : ";
            cin >> senha;
            s.setSenha(senha);
        break;
        }catch(const invalid_argument &e){
            cout << "\tEntrada em formato incorreto" << endl;
            cout << "\t" << e.what() << endl;
        }
    };
    cout << "saiu";
    //Aciona contraladora de negocios
    resultado = MNCntrAutenticacao->autenticar(m,s);

    if( resultado.getResultado() == ResultadoAutenticacao::SUCESSO ){
        cout << "SUCESSO na autenticacao" << endl;
    }else{
        cout << "FALHA na autenticacao" << endl;
    };

    return resultado;
};

void MACntrAutenticacao::setCntrAutenticacao(IMNAutenticacao *controladora){
    this->MNCntrAutenticacao = controladora;
};
