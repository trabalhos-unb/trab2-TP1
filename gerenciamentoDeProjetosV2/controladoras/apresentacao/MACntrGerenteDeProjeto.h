#ifndef MACNTRGERENTEDEPROJETO_H
#define MACNTRGERENTEDEPROJETO_H

#include <iostream>
#include <stdexcept>

#include "../../include/Entidades.h"
#include "../../include/Dominios.h"
#include "../../interfaces/Interface_Apresentacao.h"

using namespace std;

/**
\brief Classe que modela a controladora de GerenteDeProjeto na camada de apresentação
\ingroup Pessoas
*/

class MACntrGerenteDeProjeto:public IMAEmpregados{
    public:
        void exibirInformacoesPessoais();
        void editarInformacoesPessoais();
        void menuPrincipal();

        void cadastrarProjeto();
        void descadastrarProjeto();
        void cadastrarDesenvolvedor();
        void descadastrarDesenvolvedor();
        void associarDesenvolvedorComProjeto();

        static MACntrGerenteDeProjeto* instanciar();
    private:
        MACntrGerenteDeProjeto();
        static MACntrGerenteDeProjeto* referenciaCntr;
        static GerenteDeProjeto* gerenteProjeto;
};

#endif // MACNTRGERENTEDEPROJETO_H
