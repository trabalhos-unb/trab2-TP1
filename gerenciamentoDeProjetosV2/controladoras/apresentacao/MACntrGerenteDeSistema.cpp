#include "MACntrGerenteDeSistema.h"

MACntrGerenteDeSistema* MACntrGerenteDeSistema::referencia_cntr = 0;
GerenteDeSistema* MACntrGerenteDeSistema::gerente_sistema = new GerenteDeSistema();

MACntrGerenteDeSistema::MACntrGerenteDeSistema(){}

void MACntrGerenteDeSistema::exibirInformacoesPessoais(){
    cout << "Nome: " << gerente_sistema->getNome() << endl;
    cout << "Matricula: " << gerente_sistema->getMatricula() << endl;
}

void MACntrGerenteDeSistema::editarInformacoesPessoais(){

}

void MACntrGerenteDeSistema::menuPrincipal(){
    int opcao;
    const int SAIR = 5;
    do{
        cout << "1| Exibir informações pessoais" << endl;
        cout << "2| Editar informações pessoais" << endl;
        cout << "3| Cadastrar Gerente de Projeto" << endl;
        cout << "4| Descadastrar Gerente de Projeto" << endl;
        cout << "5| Sair do sistema" << endl;
        cout << ">> ";

        cin >> opcao;

        switch( opcao ){
            case 1:
                exibirInformacoesPessoais();
            break;
            case 2:
                editarInformacoesPessoais();
            break;
            case 3:
                cadastrarGerenteProjeto();
            break;
            case 4:
                descadastrarGerenteProjeto();
            break;
            case SAIR:
                //Sair
            break;
        };
    }while( opcao != SAIR );

}

void MACntrGerenteDeSistema::cadastrarGerenteProjeto(){
    //Declaracao de variaveis
    string nome_s;
    string matricula_s;
    string senha_s;
    Nome nome;
    Matricula matricula;
    Senha senha;

    try{
        cout << "Entre com o Nome : ";
        cin >> nome_s;
        nome.setNome(nome_s);
        cout << "Entre com a Matricula : ";
        cin >> matricula_s;
        matricula.setMatricula(matricula_s);
        cout << "Entre com a Senha : ";
        cin >> senha_s;
        senha.setSenha(senha_s);
    }catch(invalid_argument e){
        cout << e.what();
    }
}

void MACntrGerenteDeSistema::descadastrarGerenteProjeto(){
    //Declaracao de variaveis
    string matricula_s;
    Matricula matricula;

    try{
        cout << "Entre com a Matricula : ";
        cin >> matricula_s;
        matricula.setMatricula(matricula_s);
    }catch(invalid_argument e){
        cout << e.what();
    }
}
