#ifndef MACNTRDESENVOLVEDOR_H
#define MACNTRDESENVOLVEDOR_H

using namespace std;

#include <stdexcept>

#include "../../include/Dominios.h"
#include "../../include/Entidades.h"

#include "../../interfaces/Interface_Apresentacao.h"
#include "../../interfaces/Interface_Negocio.h"

/**
\brief Classe que modela a controladora de Desenvolvedor na camada de apresentação
\ingroup Pessoas
*/
class MACntrDesenvolvedor:public IMADesenvolvedor{
    public:
        void editarInformacoesPessoais();
        void exibirInformacoesPessoais();
        void editarInformacoesProjetos();
        void menuPrincipal();
        void setCntrDesenvolvedor(IMNDesenvolvedor*);
    private:
        Desenvolvedor* desenvolvedor;
        IMNDesenvolvedor* negocio_desenvolvedor;
};

#endif // MACNTRDESENVOLVEDOR_H
