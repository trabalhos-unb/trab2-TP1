#ifndef MACNTRPROJETO_H
#define MACNTRPROJETO_H

#include <iostream>
#include <string>
#include <stdexcept>

#include "../../interfaces/Interface_Apresentacao.h"
#include "../../interfaces/Interface_Negocio.h"
#include "../../include/Dominios.h"

using namespace std;

/**
\brief Classe que modela a camada de négócio do Módulo de Projeto
\ingroup Projeto
*/

class MACntrProjeto:public IMAProjeto{
    public:
        ///Metodo da interface que executa as opcoes incluir, editar, remover e pesquisar
        void executar(const Matricula&);
        ///Estabelecer relação entre controladora de projeto com a de negócio
        void  setCntrProjeto(IMNProjeto *);

    private:
        const static int INCLUIR;
        const static int REMOVER;
        const static int PESQUISAR;
        const static int EDITAR;
        const static int RETORNAR;

        IMNProjeto *cntrMNProjeto;
        void incluir() throw (runtime_error);
        void remover() throw (runtime_error);
        void pesquisar() throw (runtime_error);
        void editar() throw (runtime_error);
};

#endif // MACNTRPROJETO_H
