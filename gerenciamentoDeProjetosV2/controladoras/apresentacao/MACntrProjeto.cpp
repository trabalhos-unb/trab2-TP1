#include "MACntrProjeto.h"
const int MACntrProjeto::INCLUIR = 1;
const int MACntrProjeto::REMOVER = 2;
const int MACntrProjeto::PESQUISAR = 3;
const int MACntrProjeto::EDITAR = 4;
const int MACntrProjeto::RETORNAR = 5;

void MACntrProjeto::incluir() throw (runtime_error){
    ResultadoProjeto resultado;

    string nome, nomeGerente;
    string codigoProjeto, telefoneGerente;
    string dataInicio, dataTermino;
    float custoAtual, custoPrevisto;
    int estado, matriculaGerente, senhaGerente;
    string continuar;

    Projeto projeto;

    while(true){

        try{

            cout << "Incluir projeto\n\n";
            cout << "Nome para o projeto (A-Z máx.20): ";
            cin >> nome;
            projeto.setNome(nome);
            cout << "Codigo do projeto (5 letras A-Z): ";
            cin >> codigoProjeto;
            projeto.setCodigo(codigoProjeto);
            cout << "Data de inicio (DD/MM/AAAA): ";
            cin >> dataInicio;
            projeto.setDataInicio(dataInicio);
            cout << "Data de termino (DD/MM/AAAA): ";
            cin >> dataTermino;
            projeto.setDataTermino(dataTermino);
            cout << "Custo atual: ";
            cin >> custoAtual;
            projeto.setCustoAtual(custoAtual);
            cout << "Custo previsto: ";
            cin >> custoPrevisto;
            projeto.setCustoPrevisto(custoPrevisto);
            cout << "Estado (1 - Ativo 2 - Inativo): ";
            cin >> estado;
            projeto.setEstado(estado);
            /*cout << "Adicionar Gerente\n";
            cout << "Nome: ";
            cin >> nomeGerente;
            //projeto.setNomeGerente();
            cout << "Matricula: ";
            cin >> matriculaGerente;
            //projeto.setMatriculaGerente();
            cout << "Senha: ";
            cin >> senhaGerente;
            //projeto.setSenhaGerente();
            cout << "Telefone: ";
            cin >> telefoneGerente;
            //projeto.setTelefoneGerente();
            */
             break;
        }
        catch (const invalid_argument &e){
            cout << "Dado em formato incorreto" << endl;
        }
    }
    resultado = cntrMNProjeto->incluir(projeto);
    if (resultado.getResultado() == ResultadoAutenticacao::FALHA){
        cout << "Falha ao incluir o projeto." << endl;
        cout << "Digite 1 para continuar: ";
        cin >> continuar;
    }

}

void MACntrProjeto::editar() throw (runtime_error){
    string continuar;
    string nome, dataInicio, dataTermino;
    float custoAtual, custoPrevisto;
    int estado;


    ResultadoProjeto resultado;
    Projeto projeto;
    while(true){
        try{

            cout << "Editar projeto\n\n";
            cout << "Nome (A-Z max.20): ";
            cin >> nome;
            projeto.setNome(nome);
            cout << "Data de inicio (DD/MM/AAAA): ";
            cin >> dataInicio;
            projeto.setDataInicio(dataInicio);
            cout << "Data de termino (DD/MM/AAAA): ";
            cin >> dataTermino;
            projeto.setDataTermino(dataTermino);
            cout << "Custo atual: ";
            cin >> custoAtual;
            projeto.setCustoAtual(custoAtual);
            cout << "Custo previsto: ";
            cin >> custoPrevisto;
            projeto.setCustoPrevisto(custoPrevisto);
            cout << "Estado (1 - Ativo 2 - Inativo): ";
            cin >> estado;
            projeto.setEstado(estado);
            break;
        }
        catch(const invalid_argument &e){
            cout << "Dado em formato incorreto" << endl;
        }
    }
    resultado = cntrMNProjeto->editar(projeto);

    if (resultado.getResultado() == ResultadoAutenticacao::FALHA){
        cout << "Falha ao editar o projeto." << endl;
        cout << "Digite 1 para continuar: ";
        cin >> continuar;
    }

}

void MACntrProjeto::pesquisar() throw (runtime_error){
    CodigoProjeto codigo;
    string cod, continuar;
    ResultadoProjeto resultado;
    try{

        cout << "Pesquisar projeto\n\n";
        cout << "Codigo projeto: ";
        cin >> cod;
        codigo.setCodigoProjeto(cod);
    }
    catch (const invalid_argument &e){
        cout << "Dado em formato incorreto." << endl;
    }
    resultado = cntrMNProjeto->pesquisar(codigo);

    if (resultado.getResultado() == ResultadoAutenticacao::FALHA){
        cout << "Falha ao pesquisar o projeto." << endl;
        cout << "Digite 1 para continuar: ";
        cin >> continuar;
    }
}

void MACntrProjeto::remover() throw (runtime_error){
    CodigoProjeto codigo;
    string cod, continuar;
    ResultadoProjeto resultado;
    try{

        cout << "Remover projeto\n\n";
        cout << "Codigo projeto: ";
        cin >> cod;
        codigo.setCodigoProjeto(cod);
    }
    catch (const invalid_argument &e){
        cout << "Dado em formato incorreto." << endl;
    }

    resultado = cntrMNProjeto->remover(codigo);
    if (resultado.getResultado() == ResultadoAutenticacao::FALHA){
        cout << "Falha ao remover o projeto." << endl;
        cout << "Digite 1 para continuar: ";
        cin >> continuar;
    }
}


void MACntrProjeto::executar(const Matricula &matricula){
    int escolha = 0;

    do{

        cout << "GERENCIAMENTO DE PROJETOS\n\n";
        cout << INCLUIR << " - Incluir\n";
        cout << REMOVER << " - Remover\n";
        cout << PESQUISAR << " - Pesquisar\n";
        cout << EDITAR << " - Editar\n";
        cout << RETORNAR << " - Retornar\n";

        cin >> escolha;

        switch(escolha){
            case INCLUIR:
                incluir();
            break;
            case REMOVER:
                remover();
            break;
            case PESQUISAR:
                pesquisar();
            break;
            case EDITAR:
                editar();
            break;
            case RETORNAR:
            break;
        }



    }while(escolha != RETORNAR);

}

void MACntrProjeto::setCntrProjeto(IMNProjeto *controladora){
    this->cntrMNProjeto = controladora;
}

