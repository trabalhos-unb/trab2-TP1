#include "MACntrDesenvolvedor.h"

void MACntrDesenvolvedor::setCntrDesenvolvedor(IMNDesenvolvedor* cntr){
    negocio_desenvolvedor = cntr;
}

void MACntrDesenvolvedor::menuPrincipal(){
    int opcao;
    do{
        cout << "1| Exibir informações pessoais" << endl;
        cout << "2| Editar informações pessoais" << endl;
        cout << "3| Editar informações acerca de Projetos" << endl;
        cout << "4| Sair do sistema" << endl;
        cout << ">> ";

        cin >> opcao;

        switch( opcao ){
            case 1:
                exibirInformacoesPessoais();
            break;
            case 2:
                editarInformacoesPessoais();
            break;
            case 3:
                editarInformacoesProjetos();
            break;
            case 4:
                //Sair
            break;
        };
    }while( opcao != 4 );
}

void MACntrDesenvolvedor::exibirInformacoesPessoais(){


    cout << "Nome : " << desenvolvedor->getNome() << endl;
    cout << "Matricula : " << desenvolvedor->getMatricula() << endl;
    cout << "Email : " << desenvolvedor->getEmail() << endl;

    cin.get();
    cin.get();
}

void MACntrDesenvolvedor::editarInformacoesPessoais(){
    //Declaracao de variaveis
    string s_nome;
    string s_senha;
    string s_email;
    Nome nome;
    Senha senha;
    Email email;


    while( true ){
        try{
            cout << "Insira o Nome desejado." << endl;
            cout << ">> ";
            cin >> s_nome;
            nome.setNome(s_nome);

            cout << "Insira a Senha desejado." << endl;
            cout << ">> ";
            cin >> s_senha;
            senha.setSenha(s_senha);

            cout << "Insira o Email desejado." << endl;
            cout << ">> ";
            cin >> s_email;
            email.setEmail(s_email);
            break;
        }catch(invalid_argument e){
            cout << e.what();
        };
    };

    negocio_desenvolvedor->editarInformacoesPessoais( nome, senha, email);

}

void MACntrDesenvolvedor::editarInformacoesProjetos(){

}
