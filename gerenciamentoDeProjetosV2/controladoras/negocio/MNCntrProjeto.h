#ifndef MNCNTRPROJETO_H
#define MNCNTRPROJETO_H

#include "../../interfaces/Interface_Negocio.h"
#include "../../interfaces/Interface_Objetos.h"

/**
\brief Classe que modela a classe do módulo de negocio do Projeto
\ingroup Projeto
*/

class MNCntrProjeto:public IMNProjeto{
    public:
        ResultadoProjeto incluir(const Projeto&) throw(runtime_error);
        ResultadoProjeto remover(const CodigoProjeto&) throw(runtime_error);
        ResultadoProjeto pesquisar(const CodigoProjeto&) throw(runtime_error);
        ResultadoProjeto editar(const Projeto&) throw(runtime_error);

};

#endif // MNCNTRPROJETO_H
