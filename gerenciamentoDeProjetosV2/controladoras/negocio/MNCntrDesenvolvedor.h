#ifndef MNCNTRDESENVOLVEDOR_H
#define MNCNTRDESENVOLVEDOR_H

#include "../../interfaces/Interface_Negocio.h"

/**
\brief Classe que modela a controladora de Desenvolvedor na camada de Negócio
\ingroup Pessoas
*/
class MNCntrDesenvolvedor:public IMNDesenvolvedor{
    public:
        void editarInformacoesProjetos(const Matricula&, const Custo&);
        void editarInformacoesPessoais(const Nome&, const Senha&, const Email&);
};

#endif // MNCNTRDESENVOLVEDOR_H
