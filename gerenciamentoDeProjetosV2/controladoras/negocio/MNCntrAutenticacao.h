#ifndef MNCNTRAUTENTICACAO_H
#define MNCNTRAUTENTICACAO_H

#include "../../interfaces/Interface_Negocio.h"
#include "../../interfaces/Interface_Objetos.h"

using namespace std;


/**
\brief Classe que modela a controladora de Autenticação na camada de Negócio
\ingroup Pessoas
*/
class MNCntrAutenticacao:public IMNAutenticacao{
    public:
        ResultadoAutenticacao autenticar(const Matricula &,const Senha &);
    private:
};

#endif // MNCNTRAUTENTICACAO_H
