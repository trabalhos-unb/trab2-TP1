var group___dominios =
[
    [ "CodigoProjeto", "class_codigo_projeto.html", [
      [ "CodigoProjeto", "class_codigo_projeto.html#a21386c1978e38e07d010c58532b8efc6", null ],
      [ "CodigoProjeto", "class_codigo_projeto.html#a24f268611c9fac90b9919b9ee29ed49a", null ],
      [ "~CodigoProjeto", "class_codigo_projeto.html#ab3f2021ed92bf991cb0e30f25fcbc8e1", null ],
      [ "getCodigoProjeto", "class_codigo_projeto.html#a69f6765d15745908424556e7150dc186", null ],
      [ "setCodigoProjeto", "class_codigo_projeto.html#a1daac1eeea7a69cda7751f418066c610", null ]
    ] ],
    [ "Custo", "class_custo.html", [
      [ "Custo", "class_custo.html#a23e5f579d7d4b049bece9f3bf2b3b2ff", null ],
      [ "Custo", "class_custo.html#a55fb65dbb7c12f5b033c1c46bb5d8393", null ],
      [ "~Custo", "class_custo.html#a7c94a6cefac8215ef50c0e74f204b7b5", null ],
      [ "getCusto", "class_custo.html#affed079e9321c9c10cfd8cbab0b357c4", null ],
      [ "setCusto", "class_custo.html#af12346e4019fe35e3ad7b7611815497e", null ]
    ] ],
    [ "Data", "class_data.html", [
      [ "Data", "class_data.html#af11f741cb7f587e2e495452a8905a22a", null ],
      [ "Data", "class_data.html#a7e546a6e6e55f93cb621011dff413f00", null ],
      [ "getData", "class_data.html#a5762e3376997c10e5c39ed4c40637b85", null ],
      [ "setData", "class_data.html#a75a50f88bc966f20826a3959717a5acc", null ]
    ] ],
    [ "Email", "class_email.html", [
      [ "Email", "class_email.html#a2cfcfea1e55511208e7858c33f48ad9d", null ],
      [ "Email", "class_email.html#abfa7891fe9bc84c6f19e91a7fceb2fed", null ],
      [ "~Email", "class_email.html#a3e56bac4e1d6170fb5ba3ece6efb89a3", null ],
      [ "getEmail", "class_email.html#aa2898fe9d48e9bf02b32b63fb18b63b9", null ],
      [ "setEmail", "class_email.html#a47fc9d4d76a2e2a7bc502f16c1d034be", null ]
    ] ],
    [ "EstadoProjeto", "class_estado_projeto.html", [
      [ "EstadoProjeto", "class_estado_projeto.html#aa90f295ccc336e23d90f6d2558c2debb", null ],
      [ "EstadoProjeto", "class_estado_projeto.html#a7be713cc5b033e1bba38ef4483506d73", null ],
      [ "~EstadoProjeto", "class_estado_projeto.html#aa1f21b478bba4ac600df56191f294e2b", null ],
      [ "getEstadoProjeto", "class_estado_projeto.html#ac451e79930abcdc3a991c08340553027", null ],
      [ "setEstadoProjeto", "class_estado_projeto.html#a06a83964d60cd90169e17d549f6341d3", null ]
    ] ],
    [ "FaseProjeto", "class_fase_projeto.html", [
      [ "FaseProjeto", "class_fase_projeto.html#a8ba8ac85bfdaca064dc03eabf034616b", null ],
      [ "FaseProjeto", "class_fase_projeto.html#a723275c0157b8c3a689085e7486d17db", null ],
      [ "~FaseProjeto", "class_fase_projeto.html#a0cb95b6fdee56d04e8f5d63f0b501283", null ],
      [ "getFaseProjeto", "class_fase_projeto.html#ad807de253b31c2ad7197849788d0875b", null ],
      [ "setFaseProjeto", "class_fase_projeto.html#a99fb495ff8c1eeafbbf90905165f3a60", null ]
    ] ],
    [ "Funcao", "class_funcao.html", [
      [ "Funcao", "class_funcao.html#a5591fbf42b8eca3c6c58233221896082", null ],
      [ "Funcao", "class_funcao.html#a7d9d695b57316586b1a44320012ed1d9", null ],
      [ "getFuncao", "class_funcao.html#ab8f1ac09f624ddf51252692bc1fd0f0d", null ],
      [ "setFuncao", "class_funcao.html#a530bf1a4e7a4a868c586fe5385d54c26", null ]
    ] ],
    [ "Matricula", "class_matricula.html", [
      [ "Matricula", "class_matricula.html#a1bce1b1921c714f243574f671f46be55", null ],
      [ "Matricula", "class_matricula.html#ad0e207a892bf8ae631a9374d9998996c", null ],
      [ "~Matricula", "class_matricula.html#aefca5c40e1dc09e4dcfd1872dc7db00d", null ],
      [ "getMatricula", "class_matricula.html#a67336bcb3774d37bafef9ae39f1a3291", null ],
      [ "setMatricula", "class_matricula.html#af5fa061fc9bc4adeee8c0d607de9aa87", null ]
    ] ],
    [ "Nome", "class_nome.html", [
      [ "Nome", "class_nome.html#a500b022728cd437dd749bfe625a26a4d", null ],
      [ "Nome", "class_nome.html#a5a31c8dec7272e90a3d5cb59cece8c28", null ],
      [ "~Nome", "class_nome.html#ad73f169d47305f70238c5b030d1c7e58", null ],
      [ "getNome", "class_nome.html#a099de6e73b92016fbf2ecbe1b036b149", null ],
      [ "setNome", "class_nome.html#ab1507b81047efb89b50b6be0d33c08e5", null ]
    ] ],
    [ "Senha", "class_senha.html", [
      [ "Senha", "class_senha.html#ade5ef5c7f37a1dd7a3bea575fb745a46", null ],
      [ "Senha", "class_senha.html#a9f4ad62d7b5add66ec2647eb3b2aef6f", null ],
      [ "~Senha", "class_senha.html#a40f5427be43fd58d37028706d5e3dc50", null ],
      [ "getSenha", "class_senha.html#ac545df9d1b3255629497c771e68e9ea4", null ],
      [ "setSenha", "class_senha.html#a735e4bf5f65cc8d28daa7dbf202fd999", null ]
    ] ],
    [ "Telefone", "class_telefone.html", [
      [ "Telefone", "class_telefone.html#a9611a4bc0ee29b63e8246c1d80cb30f3", null ],
      [ "Telefone", "class_telefone.html#a0e07636a72110b0f74bd92c07a25dcce", null ],
      [ "~Telefone", "class_telefone.html#a307a04e70704548ed745fce4744acd58", null ],
      [ "getTelefone", "class_telefone.html#a8bbb4c487f0bd84f07486c0c5339d918", null ],
      [ "setTelefone", "class_telefone.html#a79516b37434ff927bd2a9bd66080a36d", null ]
    ] ]
];