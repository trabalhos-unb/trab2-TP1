#ifndef TELAS_H_INCLUDED
#define TELAS_H_INCLUDED

#include <iostream>

using namespace std;

class Telas{
    public:
        void logo() const{
            //
            cout << "############################################" << endl;
            cout << "#  ______          ____  ___               #" << endl;
            cout << "#  | ___ \\        (_)  \\/  |               #" << endl;
            cout << "#  | |_/ / __ ___  _| .  . | __ _ _ __     #" << endl;
            cout << "#  |  __/ '__/ _ \\| | |\\/| |/ _` | '_ \\    #" << endl;
            cout << "#  | |  | | | (_) | | |  | | (_| | | | |   #" << endl;
            cout << "#  \\_|  |_|  \\___/| \\_|  |_/\\__,_|_| |_|   #" << endl;
            cout << "#                _/ |                      #" << endl;
            cout << "#               |__/                       #" << endl;
            cout << "#                                          #" << endl;
            cout << "############################################" << endl;
        };
        void sair() const{

            cout << "Obrigado pela confiança em utilizar nosso sistema!!!" << endl;
        };
};

#endif // TELAS_H_INCLUDED
