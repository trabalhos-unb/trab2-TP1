#include "./builders.h"

IMAAutenticacao* BuilderAutenticacao::build(){
    IMNAutenticacao* negocio_aut;
    IMAAutenticacao* apresentacao_aut;

    negocio_aut = new StubMNCntrAutenticacao();
    apresentacao_aut = new MACntrAutenticacao();

    apresentacao_aut->setCntrAutenticacao( negocio_aut );

    return apresentacao_aut;
};

IMADesenvolvedor* BuilderDesenvolvedor::build(Desenvolvedor* d){
    IMADesenvolvedor* desenvolvedor;
    IMNDesenvolvedor* desenvolvedor_negocio;

    desenvolvedor = new MACntrDesenvolvedor();
    desenvolvedor_negocio = new StubMNCntrDesenvolvedor();

    desenvolvedor->setCntrDesenvolvedor(desenvolvedor_negocio);

    return desenvolvedor;
};

IMAEmpregados* BuilderGerenteDeSistema::build(GerenteDeSistema* gs){
    IMAEmpregados* gerenteSistema;
    gerenteSistema = MACntrGerenteDeSistema::instanciar();
    return gerenteSistema;
};


IMAEmpregados* BuilderGerenteDeProjeto::build(GerenteDeProjeto* gp){
    IMAEmpregados* gerenteProjeto;
    gerenteProjeto = MACntrGerenteDeProjeto::instanciar();
    return gerenteProjeto;
};

IMAProjeto* BuilderProjeto::build(){
    IMNProjeto* negocio_proj;
    IMAProjeto* apresentacao_proj;

    negocio_proj = new StubMNCntrProjeto();
    apresentacao_proj = new MACntrProjeto();

    apresentacao_proj->setCntrProjeto(negocio_proj);
    return apresentacao_proj;
};
