#ifndef BUILDERS_H_INCLUDED
#define BUILDERS_H_INCLUDED

#include "../include/Entidades.h"

#include "../interfaces/Interface_Apresentacao.h"
#include "../interfaces/Interface_Negocio.h"

#include "../stubs/Stubs_Negocio.h"

#include "../controladoras/Controladoras_Apresentacao.h"
#include "../controladoras/Controladoras_Negocio.h"

using namespace std;

/**
\brief Constrói a controladora de Autenticacao
\ingroup Autenticacao
*/
class BuilderAutenticacao{
    public:
        static IMAAutenticacao* build();

};

/**
\brief Constrói a controladora de Desenvolvedor
\ingroup Pessoas
*/
class BuilderDesenvolvedor{
    public:
        static IMADesenvolvedor* build(Desenvolvedor*);
};

/**
\brief Constrói a controladora de GerenteDeSistema
\ingroup Pessoas
*/
class BuilderGerenteDeSistema{
    public:
        static IMAEmpregados* build(GerenteDeSistema*);
};

/**
\brief Constrói a controladora de GerenteDeProjeto
\ingroup Pessoas
*/
class BuilderGerenteDeProjeto{
    public:
        static IMAEmpregados* build(GerenteDeProjeto*);
};

/**
\brief Constrói a controladora de Projeto
\ingroup Projeto
*/

class BuilderProjeto{
    public:
        static IMAProjeto* build();
};

#endif // BUILDERS_H_INCLUDED
