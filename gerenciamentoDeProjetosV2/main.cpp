#include <iostream>

#include "./include/Dominios.h"
#include "./include/Entidades.h"

#include "./interfaces/Interface_Apresentacao.h"
#include "./interfaces/Interface_Objetos.h"

#include "./controladoras/Controladoras_Apresentacao.h"
#include "./controladoras/Controladoras_Negocio.h"
#include "./controladoras/Controladoras_Persistencia.h"

#include "./stubs/Stubs_Apresentacao.h"
#include "./stubs/Stubs_Negocio.h"
#include "./stubs/Stubs_Persistencia.h"

#include "./builders/builders.h"
#include "./Telas.h"

using namespace std;

Telas *tela = new Telas();

int main(){
    //Declaracao de variaveis
    IMAAutenticacao* autenticacao;
    IMAEmpregados* empregado;
    IMAProjeto* projeto;
    ResultadoAutenticacao resultadoAutenticacao;
    Matricula m;
    //Definicao de variaveis
    autenticacao = BuilderAutenticacao::build();
    projeto = BuilderProjeto::build();

    //Tela Inicial
    tela->logo();
    try{
        resultadoAutenticacao = autenticacao->autenticar();


    switch( resultadoAutenticacao.getPerfil() ){
        case IMAEmpregados::DESENVOLVEDOR:
            empregado = BuilderDesenvolvedor::build( resultadoAutenticacao.getDesenvolvedor() );
            m.setMatricula(resultadoAutenticacao.getDesenvolvedor()->getMatricula());
        break;
        case IMAEmpregados::GERENTE_PROJETO:
            empregado = BuilderGerenteDeProjeto::build( resultadoAutenticacao.getGerenteProjeto() );
            m.setMatricula(resultadoAutenticacao.getGerenteProjeto()->getMatricula());
        break;
        case IMAEmpregados::GERENTE_SISTEMAS:
            empregado = BuilderGerenteDeSistema::build( resultadoAutenticacao.getGerenteSistema() );
            m.setMatricula(resultadoAutenticacao.getGerenteSistema()->getMatricula());
        break;
    };
    } catch (invalid_argument e){
        cout << e.what() << endl;
    }
    tela->logo();

    empregado->menuPrincipal();
    projeto->executar(m);
    //cout << m.getMatricula();

    tela->sair();

//    IUProjeto *cntrIUProjeto = new CntrIUProjeto();
//    cntrIUProjeto->executar();

    return 0;
};
