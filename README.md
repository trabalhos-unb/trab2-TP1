# trab2-TP1

## Funcionalidades Necessária
* Cadastar Gerente de Sistema
* Editar informação Gerente de Sistema
\s\s
* Cadastar Gerente de Projeto
* Editar informação Gerente de Projeto
* Excluir Gerente de Projeto
  ->Apenas se projetos que gerencia estiverem inativos
\s\s
* Cadastar Desenvolvedor
* Editar informação Desenvolvedor
* Excluir Desenvolvedor
  ->Apenas se projetos que participa estiverem inativos
\s\s
* Cadastar Projeto
* Editar informação Projeto
  ->Não é possivel editar informaçao de projeto excerrado
\s\s
* Alocar e desalocar desenvolvedores a projetos
\s\s
* Exibe informações sobre pessoa a partir da matricula ou após logar?
* Exibe informações sobre projeto a partir do código

### Autenticacao
* Apenas gerente de sistema cadastra e excluir gerentes de projeto
* Apenas gerente de projeto cadastra e excluir projetos
* Apenas gerente de projeto cadastra , excluir , aloca e descaloca desenvolvedores

### Gerente de Projeto
* Pode gerenciar no máximo 5 projetos
* Pode editar informação pessoal, exceto matricula
* Pode editar informação de projeto, exceto código

### Desenvolvedor 
* Pode participar de no máximo 10 projetos
* Pode editar informação pessoal, exceto matricula

